# -*- coding: utf-8 -*-
#
# Copyright (C) 2016 John Hampton <pacopablo@pacopablo.com>
# All rights reserved.
#
# This software is licensed as described in the file COPYING, which
# you should have received as part of this distribution.
#
# Author: John Hampton <pacopablo@pacopablo.com>

import uptimerobot.account

TEST_DATA = {
    "stat": "ok",
    "account": {
        "monitorLimit": "100",
        "monitorInterval": "1",
        "upMonitors": "35",
        "downMonitors": "9",
        "pausedMonitors": "11"
    }
}


def test_account_initialization():
    """ Test Account object instantiation """

    attributes = {
        'account_status': TEST_DATA['stat'],
        'monitor_limit': int(TEST_DATA['account']['monitorLimit']),
        'monitor_interval': int(TEST_DATA['account']['monitorInterval']),
        'up_monitors': int(TEST_DATA['account']['upMonitors']),
        'down_monitors': int(TEST_DATA['account']['downMonitors']),
        'paused_monitors': int(TEST_DATA['account']['pausedMonitors']),
    }

    acct = uptimerobot.account.Account(TEST_DATA)
    for attr in attributes.keys():
        assert getattr(acct, attr) == attributes[attr]