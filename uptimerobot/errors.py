# -*- coding: utf-8 -*-
#
# Copyright (C) 2016 John Hampton <pacopablo@pacopablo.com>
# All rights reserved.
#
# This software is licensed as described in the file COPYING, which
# you should have received as part of this distribution.
#
# Author: John Hampton <pacopablo@pacopablo.com>


__all__ = [
    'UptimeRobotInvalidAPIMethod',
    'UptimeRobotNoAPIKey',
    'UptimeRobotInvalidAPIVersion',
]


class UptimeRobotInvalidAPIMethod(Exception):
    """ Raised when an invalid API method is called """

    def __init__(self, method=''):
        self.method = method

    def __repr__(self):
        return "UptimeRobotInvalidAPIMethod('{}')".format(self.method)

    def __str__(self):
        return "An invalid API method was called.  " \
                "{} is not a valid API method".format(self.method)


class UptimeRobotNoAPIKey(Exception):
    """ Raised when trying to call API method w/o first setting apiKey """

    def __repr__(self):
        return "UptimeRobotNoAPIKey()"

    def __str__(self):
        return "No API key has been set. Please call set_api_key() first"


class UptimeRobotInvalidAPIVersion(Exception):
    """ Raised when trying to call API method w/o first setting apiKey """

    def __init__(self, version=''):
        self.version = version

    def __repr__(self):
        return "UptimeRobotInvalidAPIVersion(version={})".format(self.version)

    def __str__(self):
        return "Version {} of the UptimeRobot API is invalid".format(
               self.version)

